require 'rails_helper'

RSpec.describe 'Cards endpoint', type: :request do
    include Response
    let!(:user){create(:user)}
    let!(:deck) {create(:deck, user: user)}
    let!(:cards){create_list(:card,10, deck: deck)}

    describe 'GET /decks/:id/cards' do
        
        let (:url){"/decks/#{deck.id}/cards"}
        
        context 'when user is unauthenticated' do
            
            before{get url}
            
            it 'returns Unauthorized' do
                expect(response).to have_http_status(401)
            end
            
            it 'Displays message to login' do
                expect(response.body).to match(/You need to sign in or sign up before continuing./)
            end
        end
        
        context 'if user is autheticated' do
            
            before{ 
                sign_in user
                get url
            }
            
            it 'returns status ok' do      
                expect(response).to have_http_status(200) 
            end
            
            it 'should return a list of cards' do
                expect(json).not_to be_empty
                expect(json.size).to eq(10)
            end
            
            it 'returns all cards that belongs to a deck' do 
                expect(response.body).to eq(cards.to_json)
            end
        end
    end
    
    describe 'POST /decks/:id/cards' do
        let (:url){"/decks/#{deck.id}/cards"}
        let(:valid_attr){{front: 'eaer', back: 'hehhe' }}
        
        context 'when user is unauthenticated' do
            
            before{get url}
            
            it 'returns Unauthorized' do
                expect(response).to have_http_status(401)
            end
            
            it 'Displays message to login' do
                expect(response.body).to match(/You need to sign in or sign up before continuing./)
            end
        end
        
        context 'if user is autheticated and card is valid' do
            
            before{ 
                sign_in user
                post url, params: valid_attr
            }
            
            it 'returns status Created' do      
                expect(response).to have_http_status(201) 
            end
            
            it 'should return the new card' do
                expect(json).not_to be_empty
                expect(json['front']).to eq 'eaer'
                expect(json['back']).to eq 'hehhe'
            end
         
            it 'card should belong to current deck' do 
                expect(json['deck_id']).to eq(deck.id)
            end
        end
        
        context 'if user is autheticated and card is not valid' do
         
            let(:invalid_param){{front: ''}}    
            before{ 
                sign_in user
                post url, params: invalid_param
            }
            
            it 'returns Unprocessable entity' do      
                expect(response).to have_http_status(422) 
            end
            
            it ' returns error message' do
                expect(response.body).to match(/Validation failed:/)
            end
        end
    end

    describe 'GET /decks/:id/cards/:id' do
        let(:card_id){deck.cards.first.id} 
        let (:url){"/decks/#{deck.id}/cards/#{card_id}"}
        
        context 'when user is unauthenticated' do
            
            before{get url}
            
            it 'returns Unauthorized' do
                expect(response).to have_http_status(401)
            end
            
            it 'Displays message to login' do
                expect(response.body).to match(/You need to sign in or sign up before continuing./)
            end
        end
        
        context 'if user is autheticated and record exists' do
            
            before{ 
                sign_in user
                get url
            }
            
            it 'returns status ok' do      
                expect(response).to have_http_status(200) 
            end
            
            it 'should return a decks' do
                expect(json).not_to be_empty
                expect(json['id']).to eq(card_id)
            end
            
            it ' returns all decks that belongs to user' do 
                expect(response.body).to eq(deck.cards.find(card_id).to_json)
            end
        end
        
        context 'if user is autheticated and record does not exist' do
         
            let(:card_id){100000}    
            before{ 
                sign_in user
                get url
            }
            
            it 'returns status Not Found' do      
                expect(response).to have_http_status(404) 
            end
            
            it ' returns error message' do 
                expect(response.body).to match(/Couldn't find Card/)
            end
        end
    end
    describe 'PUT /decks/:id/cards/:id' do
        let(:card_id){deck.cards.first.id} 
        let (:url){"/decks/#{deck.id}/cards/#{card_id}"}
        let(:valid_attr){{back: 'haaaaaaaa' }}
        
        context 'when user is unauthenticated' do
            
            before{get url}
            
            it 'returns Unauthorized' do
                expect(response).to have_http_status(401)
            end
            
            it 'Displays message to login' do
                expect(response.body).to match(/You need to sign in or sign up before continuing./)
            end
        end
        
        context 'if user is autheticated and change is valid' do
            
            before{ 
                sign_in user
                put url, params: valid_attr
            }
            
            it 'returns status ' do
                expect(response).to have_http_status(200) 
            end
            
            it 'should return the modificated deck' do
                expect(json).not_to be_empty
                expect(json['id']).to eq cards.first.id
                expect(json['front']).to eq cards.first.front
                expect(json['deck_id']).to eq cards.first.deck_id
                expect(json['back']).to eq 'haaaaaaaa'
            end
        end
        
        context 'if user is autheticated and change is not valid' do
         
            let(:invalid_param){{front: ''}}    
            before{ 
                sign_in user
                put url, params: invalid_param
            }
            
            it 'returns Unprocessable entity' do      
                expect(response).to have_http_status(422) 
            end
            it 'record remain the same' do
                expect(deck.cards.find(card_id)['front']).not_to eq('')
            end
        end
    end
    describe ' /decks/:id/cards/:id' do
        let (:card_id) {cards.first.id}
        let (:url){"/decks/#{deck.id}/cards/#{card_id}"}
        context 'when user is unauthenticated' do
            
            before{get url}
            
            it 'returns Unauthorized' do
                expect(response).to have_http_status(401)
            end
            
            it 'Displays message to login' do
                expect(response.body).to match(/You need to sign in or sign up before continuing./)
            end
            
            it 'does not delete the record' do
                expect(cards.first.id).to eq(card_id)
            end
        end
        context 'when user is authorized' do
            
            before{ 
                sign_in user
                delete url
            }
            
            it 'returns No Content' do
                expect(response).to have_http_status(204)
            end
            
            it 'deletes the record' do
                expect{deck.cards.find(card_id)}.to raise_exception(ActiveRecord::RecordNotFound) 
            end
        end
    end
end
