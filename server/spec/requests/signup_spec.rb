require 'rails_helper'

RSpec.describe 'POST /signup', type: :request do
  let(:url) { '/signup' }
  let(:params) do
    {
      user: {
        email: 'user@example.com',
        name: 'John Doe',
        password: 'password',
        password_confirmation: 'password'
      }
    }
  end

  context 'when user is unauthenticated' do
    before { post url, params: params }

    it 'returns 200' do
      expect(response).to have_http_status(201)
    end

    it 'returns a new user' do
    #  expect(response.body).to match_schema('user')
    end
  end

  context 'when user already exists' do
    let(:user){create(:user)}
    let(:params) do
        {
            user: {
                email: user.email,
                name: user.name,
                password: user.password,
                password_confirmation: user.password_confirmation
            }
        }
    end
    before do
      post url, params: params
    end

    it 'returns bad request status' do
      expect(response).to have_http_status(422)
    end

    it 'returns validation errors' do
    #    expect(response.body['errors'].first['title']).to eq('Bad Request')
    end
  end
end
