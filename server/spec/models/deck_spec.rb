require 'rails_helper'

RSpec.describe Deck, type: :model do
    context 'Associations' do
        it {should belong_to(:user)}
        it {should have_many(:cards).dependent(:destroy)}
    end
    context 'Validations' do
        it {should validate_presence_of(:name)}
        it {should validate_length_of(:name).is_at_least(5)}
        it {should validate_uniqueness_of(:name).
            scoped_to(:user_id).
            case_insensitive.
            with_message("Você já possui um deck de mesmo nome")}
    end
end
