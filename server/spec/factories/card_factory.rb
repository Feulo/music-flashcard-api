FactoryBot.define do
    factory :card do
        front {Faker::Name.unique.name}
        back {Faker::StarWars.character}
        deck
    end
end
