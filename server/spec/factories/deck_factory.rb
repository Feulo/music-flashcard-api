FactoryBot.define do
    factory :deck do
        name {Faker::Internet.user_name(5)}
        user
    end
end
