class DecksController < ApplicationController

    before_action :authenticate_user!
    before_action :set_deck, only: [:show, :update, :destroy, :next_card]
    def index
        @decks= current_user.decks.all
        json_response(@decks)
    end
    
    def show
        json_response(@deck)
    end
    
    def create
        @deck = current_user.decks.create!(deck_params)
        json_response(@deck, :created)
    end
    def update 
        @deck.update!(deck_params)
        json_response(@deck, :ok)
    end

    def destroy
        @deck.destroy
        head :no_content 
    end

   
    def next_card
        @card = @deck.cards.order("RANDOM()").first #montar funcao de verdade no modelo
        json_response @card
    end
    private

    def deck_params
        params.permit(:name,:description)
    end

    def set_deck
        @deck = Deck.find(params[:id])
    end
end
