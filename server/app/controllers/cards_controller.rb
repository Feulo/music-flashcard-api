class CardsController < ApplicationController
    
    before_action :authenticate_user!

    def create
        @deck = Deck.find(params[:deck_id])
        @card = @deck.cards.create!(card_params)
        json_response @card, :created 
    end

    def update
        @card = Card.find(params[:id])
        @card.update!(card_params)
        json_response @card, :ok
    end

    def destroy
        @card = Card.find(params[:id])
        @card.destroy
        head :no_content
    end

    def show
        @card = Card.find(params[:id])
        json_response @card
    end
    def index
        @deck = Deck.find(params[:deck_id])
        @cards = @deck.cards.all
        json_response @cards
    end
    private
    def card_params
        params.permit(:front, :back)
    end
end
