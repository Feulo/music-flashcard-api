class Card < ApplicationRecord
  belongs_to :deck
  validates_presence_of :front, :back
  validates_uniqueness_of :front, :scope => [:deck_id],
        :case_sensitive => false
end
